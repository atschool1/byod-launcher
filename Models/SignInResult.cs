using System;

namespace ByodLauncher.Models
{
    public record SignInResult(bool Success, ByodUser User, string AccessToken, string RefreshToken)
    {
        public SignInResult() : this(false, null, String.Empty, String.Empty)
        {
            // Empty body
        }
    }
}