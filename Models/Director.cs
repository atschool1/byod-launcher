using System;
using System.Collections.Generic;

namespace ByodLauncher.Models
{
    public class Director : ByodUser
    {
        [Obsolete("Use first and last name instead")]
        public string DisplayName => $"{FirstName} {LastName}";

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ConnectionId { get; set; }

        public ICollection<Session> Sessions { get; set; }
    }
}