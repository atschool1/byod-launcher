using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ByodLauncher.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace ByodLauncher.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ByodLauncherContext _context;

        public AuthenticationController(IConfiguration configuration, ByodLauncherContext context)
        {
            _configuration = configuration;
            _context = context;
        }

        [HttpGet("authenticateDirector")]
        public async Task<IActionResult> AuthenticateDirector([FromQuery] Guid sessionId)
        {
            DirectorSession session = await _context.DirectorSessions
                                                    .Include(session => session.Director)
                                                    .FirstAsync(session => session.Id == sessionId);

            if (session == null || session.CreationDate.AddSeconds(60) < DateTime.Now)
            {
                return BadRequest();
            }

            var claims = new List<Claim>
            {
                new(ClaimTypes.Name, $"{session.Director.FirstName} {session.Director.LastName}")
            };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(new[] {claimsIdentity});
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal);

            return Ok();
        }

        [HttpGet("getJwtForSessionId")]
        public async Task<IActionResult> GetJwtForSessionId([FromQuery] Guid sessionId)
        {
            DirectorSession session = await _context.DirectorSessions
                                                    .Include(session => session.Director)
                                                    .FirstAsync(session => session.Id == sessionId);

            if (session == null || session.CreationDate.AddSeconds(60) < DateTime.Now)
            {
                return BadRequest();
            }

            var token = GenerateJwtToken(session.Director);

            return Ok(token);
        }

        [HttpPost]
        [EnableCors("_localhostCorsPolicy")]
        public async Task<IActionResult> RequestAccess(
            [FromHeader(Name = "X-First")] string firstName,
            [FromHeader(Name = "X-Last")] string lastName,
            [FromHeader(Name = "X-UserId")] Guid directorId
        )
        {
            Director director = await _context.Directors.FindAsync(directorId);
            // Create new director if not yet existing
            if (director is null)
            {
                director = new Director {FirstName = firstName, LastName = lastName, Id = directorId};
                await _context.Directors.AddAsync(director);
                // await _context.SaveChangesAsync();
            }

            // Create new session for director
            Guid sessionId = Guid.NewGuid();
            while (await _context.DirectorSessions.AnyAsync(session => session.Id == sessionId))
            {
                sessionId = Guid.NewGuid();
            }

            await _context.DirectorSessions.AddAsync(new DirectorSession
            {
                Id = sessionId,
                Director = director,
                CreationDate = DateTime.Now
            });

            await _context.SaveChangesAsync();

            return Redirect($"/director-authentication?sessionId={sessionId}");
        }

        private bool DirectorExists(Guid directorId)
        {
            return _context.Directors.Any(director => director.Id == directorId);
        }

        [HttpGet("authenticatedRequest")]
        [Authorize]
        public async Task<IActionResult> AuthenticatedRequestTest()
        {
            return Ok($"{HttpContext.User}");
            return Ok("Authentication successful");
        }

        [HttpGet]
        public async Task<IActionResult> Login([FromQuery] string username, [FromQuery] string authToken)
        {
            var expectedAuthToken = _configuration["Authentication:DummyAuthenticationToken"];
            if (!string.IsNullOrEmpty(username) && expectedAuthToken.Equals(authToken))
            {
                var claims = new List<Claim>
                {
                    new(ClaimTypes.Name, username)
                };
                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(new[] {claimsIdentity});
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal);
            }
            else
            {
                return BadRequest();
            }

            return Ok();
        }

        private string GenerateJwtToken(Director director)
        {
            var key = _configuration["Authentication:Jwt:Key"];
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, $"{director.Id}"),
                    new Claim("name", $"{director.FirstName} {director.LastName}"),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                }),
                
                Issuer = _configuration["Authentication:Jwt:Issuer"],
                Audience = _configuration["Authentication:Jwt:Audience"],
                Expires = DateTime.Now.AddDays(2),
                SigningCredentials = credentials
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}