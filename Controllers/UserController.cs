using System.Threading.Tasks;
using ByodLauncher.Models;
using Microsoft.AspNetCore.Mvc;

namespace ByodLauncher.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController
    {
        private readonly ByodLauncherContext _context;

        public UserController(ByodLauncherContext context)
        {
            _context = context;
        }

    }
}