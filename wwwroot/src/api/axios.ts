import axios from 'axios'

const apiDomain = process.env.VUE_APP_API_HOST;
const apiPath = process.env.VUE_APP_API_PATH;

const axiosInstance = axios.create({
    baseURL: `${apiDomain}${apiPath}`,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
    timeout: 7500,
});

export default axiosInstance;