import Api from "@/api/api";
import {AxiosError, AxiosResponse} from "axios";
import axiosInstance from "@/api/axios";

class AuthApi extends Api {

    public getJwtFromSessionId(sessionId: string): Promise<string> {
        return this.get<string>(`authentication/getJwtForSessionId`, {params: {sessionId}})
            .then((response: AxiosResponse) => {
                const token = response.data;
                axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
                return token;
            })
            .catch((error: AxiosError) => {
                throw error;
            });
    }

    public MakeAuthenticatedRequest(): Promise<string> {
        return this.get<string>(`authentication/authenticatedRequest`)
            .then((response: AxiosResponse) => {
                return response.data;
            })
            .catch((error: AxiosError) => {
                throw error;
            });
    }

}

export const authApi = new AuthApi();